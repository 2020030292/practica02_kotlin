package com.example.practica02_kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import java.text.DecimalFormat

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val txtAltura = findViewById<EditText>(R.id.txtAltura) as EditText;
        val txtPeso=findViewById<EditText>(R.id.txtPeso) as EditText;
        val lblIMC=findViewById<TextView>(R.id.lblIMC)as TextView;
        val btnCalcular=findViewById<Button>(R.id.btnCalcular) as Button;
        val btnRegresar=findViewById<Button>(R.id.btnRegresar) as Button;
        val btnLimpiar=findViewById<Button>(R.id.btnLimpiar) as Button;


        var altcm = 0.0;
        var kg = 0.0;
        var imc = 0.0;

        btnLimpiar.setOnClickListener(View.OnClickListener {
            txtPeso.setText("");
            lblIMC.setText("");
            txtAltura.setText("");
        })


        btnCalcular.setOnClickListener(View.OnClickListener {
             if(txtAltura.text.toString().equals("") || txtPeso.text.toString().equals(""))
            {
                Toast.makeText(this@MainActivity,"Faltó capturar información", Toast.LENGTH_SHORT).show();
            }
            else{
               var num1 = txtAltura.text.toString().toDouble();
                 var num2 = txtPeso.text.toString().toDouble();
                 var res = num2/(num1 * num1);

                 val imc = (Math.round(res * 10.0) / 10.0)

                lblIMC.setText("El IMC es: $imc");

            }

        });

        btnRegresar.setOnClickListener(View.OnClickListener {

            finish();
        })
    }
}